//===- StencilInductionVars.h ---------------------------------------------===//
//
// Copyright 2019 The MLIR Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================

#ifndef MLIR_STENCIL_STENCILINDUCTIONVARS_H_
#define MLIR_STENCIL_STENCILINDUCTIONVARS_H_

#include "llvm/ADT/SmallVector.h"

namespace mlir {
class Operation;
class Value;

namespace Stencil {

llvm::SmallVector<Value *, 3> getInductionVars(Operation *op);

} // namespace Stencil
} // namespace mlir

#endif // MLIR_STENCIL_STENCILINDUCTIONVARS_H_

// RUN: mlir-opt %s -verify-diagnostics | mlir-opt -verify-diagnostics | FileCheck %s

func @add_f32(%arg0 : f32, %arg1 : f32) -> f32 {
  %0 = C.addf(%arg0, %arg1) : f32
  return %0 : f32
}

// CHECK:       func @add_f32(%arg0: f32, %arg1: f32) -> f32 {
// CHECK-NEXT:    %0 = C.addf(%arg0, %arg1) : f32
// CHECK-NEXT:    return %0 : f32
// CHECK-NEXT:  }

func @sub_f32(%arg0 : f32, %arg1 : f32) -> f32 {
  %0 = C.subf(%arg0, %arg1) : f32
  return %0 : f32
}

// CHECK:       func @sub_f32(%arg0: f32, %arg1: f32) -> f32 {
// CHECK-NEXT:    %0 = C.subf(%arg0, %arg1) : f32
// CHECK-NEXT:    return %0 : f32
// CHECK-NEXT:  }

func @mul_f32(%arg0 : f32, %arg1 : f32) -> f32 {
  %0 = C.mulf(%arg0, %arg1) : f32
  return %0 : f32
}

// CHECK:       func @mul_f32(%arg0: f32, %arg1: f32) -> f32 {
// CHECK-NEXT:    %0 = C.mulf(%arg0, %arg1) : f32
// CHECK-NEXT:    return %0 : f32
// CHECK-NEXT:  }

func @div_f32(%arg0 : f32, %arg1 : f32) -> f32 {
  %0 = C.divf(%arg0, %arg1) : f32
  return %0 : f32
}

// CHECK:       func @div_f32(%arg0: f32, %arg1: f32) -> f32 {
// CHECK-NEXT:    %0 = C.divf(%arg0, %arg1) : f32
// CHECK-NEXT:    return %0 : f32
// CHECK-NEXT:  }

func @add_f64(%arg0 : f64, %arg1 : f64) -> f64 {
  %0 = C.addf(%arg0, %arg1) : f64
  return %0 : f64
}

// CHECK:       func @add_f64(%arg0: f64, %arg1: f64) -> f64 {
// CHECK-NEXT:    %0 = C.addf(%arg0, %arg1) : f64
// CHECK-NEXT:    return %0 : f64
// CHECK-NEXT:  }

func @add_int32(%arg0 : i32, %arg1 : i32) -> i32 {
  %0 = C.addi(%arg0, %arg1) : i32
  return %0 : i32
}

// CHECK:       func @add_int32(%arg0: i32, %arg1: i32) -> i32 {
// CHECK-NEXT:    %0 = C.addi(%arg0, %arg1) : i32
// CHECK-NEXT:    return %0 : i32
// CHECK-NEXT:  }

func @ret_voidptr(%arg0 : !C.voidptr) -> !C.voidptr {
  return %arg0 : !C.voidptr
}

// CHECK:       func @ret_voidptr(%arg0: !C.voidptr) -> !C.voidptr {
// CHECK-NEXT:    return %arg0 : !C.voidptr
// CHECK-NEXT:  }

func @call(%arg0 : f32, %arg1 : f32) -> f32 {
  %0 = call @add_f32(%arg0, %arg1) : (f32, f32) -> f32
  return %0 : f32
}

// CHECK-LABEL: func @call(%arg0: f32, %arg1: f32) -> f32 {
// CHECK-NEXT:    %0 = call @add_f32(%arg0, %arg1) : (f32, f32) -> f32
// CHECK-NEXT:    return %0 : f32
// CHECK-NEXT:  }

func @loop(%arg0: index, %arg1: index, %arg2: !C.voidptr) {
  C.for(%i = %arg0 to %arg1) {
    C.for(%j = %arg0 to %arg1) {
      %res = C.addi(%i, %j) : index
    }
  }
  return
}

// CHECK-LABEL: func @loop(%arg0: index, %arg1: index, %arg2: !C.voidptr) {
// CHECK-NEXT:    C.for (%i0 = %arg0 to %arg1) {
// CHECK-NEXT:      C.for (%i1 = %arg0 to %arg1) {
// CHECK-NEXT:        %0 = C.addi(%i0, %i1) : index
// CHECK-NEXT:      }
// CHECK-NEXT:    }
// CHECK-NEXT:    return
// CHECK-NEXT:  }

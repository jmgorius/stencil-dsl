// RUN: mlir-opt %s -verify-diagnostics | mlir-opt -verify-diagnostics | FileCheck %s

func @laplacian(%arg0: !stencil<"field:f64">) -> f64
 attributes { stencil.function } {
  %0 = stencil.constant_offset 1 0 0
  %1 = stencil.read(%arg0, %0) : f64
  %2 = stencil.constant_offset -1 0 0
  %3 = stencil.read(%arg0, %2) : f64
  %4 = stencil.add(%1, %3) : f64
  %5 = stencil.constant_offset 0 1 0
  %6 = stencil.read(%arg0, %5) : f64
  %7 = stencil.add(%4, %6) : f64
  %8 = stencil.constant_offset 0 -1 0
  %9 = stencil.read(%arg0, %8) : f64
  %10 = stencil.add(%7, %9) : f64
  %cst = constant 4.000000e+00 : f64
  %11 = stencil.constant_offset 0 0 0
  %12 = stencil.read(%arg0, %11) : f64
  %13 = stencil.mul(%cst, %12) : f64
  %14 = stencil.sub(%10, %13) : f64
  return %14 : f64
}

func @hori_diff_stencil(%arg0: !stencil<"field:f64">, %arg1: !stencil<"field:f64">) {
  %0 = stencil.temp : !stencil<"field:f64">
  %1 = stencil.context "kstart" : index
  %2 = stencil.context "kend" : index
  stencil.vertical_region(%1, %2) {

    %3 = stencil.lambda @laplacian(%arg0) : (!stencil<"field:f64">) -> f64
    %4 = stencil.constant_offset 0 0 0
    %5 = stencil.read(%3, %4) : f64
    stencil.write(%0, %5) : f64
    %6 = stencil.lambda @laplacian(%0) : (!stencil<"field:f64">) -> f64
    %7 = stencil.constant_offset 0 0 0
    %8 = stencil.read(%6, %7) : f64
    stencil.write(%arg1, %8) : f64
  }
  return
}

// CHECK-LABEL: @laplacian
// CHECK:         %0 = stencil.constant_offset 1 0 0
// CHECK-NEXT:    %1 = stencil.read(%arg0, %0) : f64
// CHECK-NEXT:    %2 = stencil.constant_offset -1 0 0
// CHECK-NEXT:    %3 = stencil.read(%arg0, %2) : f64
// CHECK-NEXT:    %4 = stencil.add(%1, %3) : f64
// CHECK-NEXT:    %5 = stencil.constant_offset 0 1 0
// CHECK-NEXT:    %6 = stencil.read(%arg0, %5) : f64
// CHECK-NEXT:    %7 = stencil.add(%4, %6) : f64
// CHECK-NEXT:    %8 = stencil.constant_offset 0 -1 0
// CHECK-NEXT:    %9 = stencil.read(%arg0, %8) : f64
// CHECK-NEXT:    %10 = stencil.add(%7, %9) : f64
// CHECK-NEXT:    %cst = constant 4.000000e+00 : f64
// CHECK-NEXT:    %11 = stencil.constant_offset 0 0 0
// CHECK-NEXT:    %12 = stencil.read(%arg0, %11) : f64
// CHECK-NEXT:    %13 = stencil.mul(%cst, %12) : f64
// CHECK-NEXT:    %14 = stencil.sub(%10, %13) : f64
// CHECK-NEXT:    return %14 : f64
// CHECK-NEXT:  }

// CHECK-LABEL: @hori_diff_stencil
// CHECK:         %0 = stencil.temp : !stencil<"field:f64">
// CHECK-NEXT:    %1 = stencil.context "kstart" : index
// CHECK-NEXT:    %2 = stencil.context "kend" : index
// CHECK-NEXT:    stencil.vertical_region(%1, %2) {
// CHECK-NEXT:      %3 = stencil.lambda @laplacian(%arg0) : (!stencil<"field:f64">) -> f64
// CHECK-NEXT:      %4 = stencil.constant_offset 0 0 0
// CHECK-NEXT:      %5 = stencil.read(%3, %4) : f64
// CHECK-NEXT:      stencil.write(%0, %5) : f64
// CHECK-NEXT:      %6 = stencil.lambda @laplacian(%0) : (!stencil<"field:f64">) -> f64
// CHECK-NEXT:      %7 = stencil.constant_offset 0 0 0
// CHECK-NEXT:      %8 = stencil.read(%6, %7) : f64
// CHECK-NEXT:      stencil.write(%arg1, %8) : f64
// CHECK-NEXT:    }
// CHECK-NEXT:    return
// CHECK-NEXT:  }

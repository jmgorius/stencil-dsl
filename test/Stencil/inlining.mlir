// RUN: mlir-opt %s --stencil-inline-lambdas | mlir-opt -verify-diagnostics | FileCheck %s

func @delta2(%off : !stencil<"offset">, %in : !stencil<"field:f64">) -> f64
 attributes { stencil.function } {
    %res = constant 42.0 : f64
    return %res : f64
}

func @delta(%off : !stencil<"offset">, %in : !stencil<"field:f64">) -> f64
 attributes { stencil.function } {
    %value = stencil.lambda @delta2(%off, %in) : (!stencil<"offset">, !stencil<"field:f64">) -> f64
    %a = constant 52.36 : f64
    %value2 = stencil.max(%a, %a) : f64
    %cond = stencil.eq(%a, %value2) : f64
    %bound = stencil.context "kstart" : index
    stencil.if %cond {
        %test = constant 42.0 : f64
        %testinner = stencil.mul(%test, %test) : f64
    } else {
        %test = constant 24 : i64
    }
    %0 = stencil.read(%in, %off) : f64
    %coff = stencil.constant_offset 0 0 0
    %1 = stencil.read(%in, %coff) : f64
    %res = stencil.sub(%0, %1) : f64
    return %res : f64
}

func @test_inlining(%in : !stencil<"field:f64">, %out : !stencil<"field:f64">) {
    %kstart = stencil.context "kstart" : index
    %kend = stencil.context "kend" : index

    stencil.vertical_region(%kstart, %kend) {
        // out = delta(i + 1, delta(j + 1, delta(k + 1, in)))
        %koff = stencil.constant_offset 0 0 1
        %0 = stencil.lambda @delta(%koff, %in) : (!stencil<"offset">, !stencil<"field:f64">) -> f64
        %joff = stencil.constant_offset 0 1 0
        %1 = stencil.lambda @delta(%joff, %0) : (!stencil<"offset">, !stencil<"field:f64">) -> f64
        %ioff = stencil.constant_offset 1 0 0
        %2 = stencil.lambda @delta(%ioff, %1) : (!stencil<"offset">, !stencil<"field:f64">) -> f64
        %off = stencil.constant_offset 0 0 0
        %3 = stencil.read(%2, %off) : f64
        stencil.write(%out, %3) : f64
    }

    return
}

// CHECK-LABEL: func @test_inlining(%arg0: !stencil<"field:f64">, %arg1: !stencil<"field:f64">) {
// CHECK:         %0 = stencil.context "kstart" : index
// CHECK-NEXT:    %1 = stencil.context "kend" : index
// CHECK-NEXT:    %2 = stencil.temp : !stencil<"field:f64">
// CHECK-NEXT:    %3 = stencil.temp : !stencil<"field:f64">
// CHECK-NEXT:    %4 = stencil.temp : !stencil<"field:f64">
// CHECK-NEXT:    %5 = stencil.temp : !stencil<"field:f64">
// CHECK-NEXT:    %6 = stencil.temp : !stencil<"field:f64">
// CHECK-NEXT:    %7 = stencil.temp : !stencil<"field:f64">
// CHECK-NEXT:    %8 = stencil.context "kstart" : index
// CHECK-NEXT:    %9 = stencil.context "kend" : index
// CHECK-NEXT:    stencil.vertical_region(%0, %1) {
// CHECK-NEXT:      %10 = stencil.constant_offset 0 0 1
// CHECK-NEXT:      %cst = constant 4.200000e+01 : f64
// CHECK-NEXT:      stencil.write(%6, %cst) : f64
// CHECK-NEXT:      %cst_0 = constant 5.236000e+01 : f64
// CHECK-NEXT:      %11 = stencil.max(%cst_0, %cst_0) : f64
// CHECK-NEXT:      %12 = stencil.eq(%cst_0, %11) : f64
// CHECK-NEXT:      %13 = stencil.context "kstart" : index
// CHECK-NEXT:      stencil.if %12 {
// CHECK-NEXT:        %cst_1 = constant 4.200000e+01 : f64
// CHECK-NEXT:        %14 = stencil.mul(%cst_1, %cst_1) : f64
// CHECK-NEXT:      } else {
// CHECK-NEXT:        %c24_i64 = constant 24 : i64
// CHECK-NEXT:      }
// CHECK-NEXT:      %15 = stencil.read(%arg0, %10) : f64
// CHECK-NEXT:      %16 = stencil.constant_offset 0 0 0
// CHECK-NEXT:      %17 = stencil.read(%arg0, %16) : f64
// CHECK-NEXT:      %18 = stencil.sub(%15, %17) : f64
// CHECK-NEXT:      stencil.write(%7, %18) : f64
// CHECK-NEXT:      %19 = stencil.constant_offset 0 1 0
// CHECK-NEXT:      %cst_2 = constant 4.200000e+01 : f64
// CHECK-NEXT:      stencil.write(%4, %cst_2) : f64
// CHECK-NEXT:      %cst_3 = constant 5.236000e+01 : f64
// CHECK-NEXT:      %20 = stencil.max(%cst_3, %cst_3) : f64
// CHECK-NEXT:      %21 = stencil.eq(%cst_3, %20) : f64
// CHECK-NEXT:      %22 = stencil.context "kstart" : index
// CHECK-NEXT:      stencil.if %21 {
// CHECK-NEXT:        %cst_4 = constant 4.200000e+01 : f64
// CHECK-NEXT:        %23 = stencil.mul(%cst_4, %cst_4) : f64
// CHECK-NEXT:      } else {
// CHECK-NEXT:        %c24_i64_5 = constant 24 : i64
// CHECK-NEXT:      }
// CHECK-NEXT:      %24 = stencil.read(%7, %19) : f64
// CHECK-NEXT:      %25 = stencil.constant_offset 0 0 0
// CHECK-NEXT:      %26 = stencil.read(%7, %25) : f64
// CHECK-NEXT:      %27 = stencil.sub(%24, %26) : f64
// CHECK-NEXT:      stencil.write(%5, %27) : f64
// CHECK-NEXT:      %28 = stencil.constant_offset 1 0 0
// CHECK-NEXT:      %cst_6 = constant 4.200000e+01 : f64
// CHECK-NEXT:      stencil.write(%2, %cst_6) : f64
// CHECK-NEXT:      %cst_7 = constant 5.236000e+01 : f64
// CHECK-NEXT:      %29 = stencil.max(%cst_7, %cst_7) : f64
// CHECK-NEXT:      %30 = stencil.eq(%cst_7, %29) : f64
// CHECK-NEXT:      %31 = stencil.context "kstart" : index
// CHECK-NEXT:      stencil.if %30 {
// CHECK-NEXT:        %cst_8 = constant 4.200000e+01 : f64
// CHECK-NEXT:        %32 = stencil.mul(%cst_8, %cst_8) : f64
// CHECK-NEXT:      } else {
// CHECK-NEXT:        %c24_i64_9 = constant 24 : i64
// CHECK-NEXT:      }
// CHECK-NEXT:      %33 = stencil.read(%5, %28) : f64
// CHECK-NEXT:      %34 = stencil.constant_offset 0 0 0
// CHECK-NEXT:      %35 = stencil.read(%5, %34) : f64
// CHECK-NEXT:      %36 = stencil.sub(%33, %35) : f64
// CHECK-NEXT:      stencil.write(%3, %36) : f64
// CHECK-NEXT:      %37 = stencil.constant_offset 0 0 0
// CHECK-NEXT:      %38 = stencil.read(%3, %37) : f64
// CHECK-NEXT:      stencil.write(%arg1, %38) : f64
// CHECK-NEXT:    }
// CHECK-NEXT:    return
// CHECK-NEXT:  }

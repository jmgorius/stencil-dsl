// RUN: mlir-opt %s -verify-diagnostics | mlir-opt -verify-diagnostics | FileCheck %s

func @init_globals() {
  stencil.declare_global "var_runtime" f64
  stencil.declare_global "var_default" f64
  %cst = constant 2.000000e+00 : f64
  stencil.set_global "var_default" %cst : f64
  stencil.declare_global "var_compiletime" i1
  return
}

func @globals_stencil(%arg0: !stencil<"field:f64">, %arg1: !stencil<"field:f64">) {
  %0 = stencil.context "kstart" : index
  %1 = stencil.context "kend" : index
  stencil.vertical_region(%0, %1) {
    %off = stencil.constant_offset 0 0 0
    %2 = stencil.read(%arg0, %off) : f64
    %3 = stencil.get_global "var_runtime" : f64
    %4 = stencil.add(%2, %3) : f64
    %5 = stencil.get_global "var_default" : f64
    %6 = stencil.add(%4, %5) : f64
    stencil.write(%arg1, %6) : f64
    %c1_f64 = constant 1.0 : f64
    stencil.set_global "var_runtime" %c1_f64 : f64
  }
  return
}

// CHECK-LABEL: @init_globals
// CHECK:    stencil.declare_global "var_runtime" f64
// CHECK-NEXT:    stencil.declare_global "var_default" f64
// CHECK-NEXT:    %cst = constant 2.000000e+00 : f64
// CHECK-NEXT:    stencil.set_global "var_default" %cst : f64
// CHECK-NEXT:    stencil.declare_global "var_compiletime" i1
// CHECK-NEXT:    return
// CHECK-NEXT:  }

// CHECK-LABEL: @globals_stencil
// CHECK:         %0 = stencil.context "kstart" : index
// CHECK-NEXT:    %1 = stencil.context "kend" : index
// CHECK-NEXT:      stencil.vertical_region(%0, %1) {
// CHECK-NEXT:        %2 = stencil.constant_offset 0 0 0
// CHECK-NEXT:        %3 = stencil.read(%arg0, %2) : f64
// CHECK-NEXT:        %4 = stencil.get_global "var_runtime" : f64
// CHECK-NEXT:        %5 = stencil.add(%3, %4) : f64
// CHECK-NEXT:        %6 = stencil.get_global "var_default" : f64
// CHECK-NEXT:        %7 = stencil.add(%5, %6) : f64
// CHECK-NEXT:        stencil.write(%arg1, %7) : f64
// CHECK-NEXT:        %cst = constant 1.000000e+00 : f64
// CHECK-NEXT:        stencil.set_global "var_runtime" %cst : f64
// CHECK-NEXT:      }
// CHECK-NEXT:    return
// CHECK-NEXT:  }

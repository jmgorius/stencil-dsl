#include "mlir/Stencil/StencilInductionVars.h"
#include "mlir/Stencil/StencilOps.h"

namespace mlir {
namespace Stencil {

SmallVector<Value *, 3> getInductionVars(Operation *op) {
  SmallVector<Value *, 3> ret(3, nullptr);
  if (!op->getBlock())
    return ret;
  if (auto parent = op->getBlock()->getContainingOp())
    ret = getInductionVars(parent);
  for (auto it = op->getBlock()->begin(); it != op->getBlock()->end(); ++it) {
    if (auto op = dyn_cast<InductionVarOp>(*it)) {
      if (op.direction() == "I")
        ret[0] = op.value();
      if (op.direction() == "J")
        ret[1] = op.value();
      if (op.direction() == "K")
        ret[2] = op.value();
    }
  }
  return ret;
}

} // namespace Stencil
} // namespace mlir

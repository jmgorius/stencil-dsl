#include "mlir/AffineOps/AffineOps.h"
#include "mlir/IR/AffineExpr.h"
#include "mlir/IR/AffineExprVisitor.h"
#include "mlir/IR/AffineMap.h"
#include "mlir/IR/BlockAndValueMapping.h"
#include "mlir/IR/Builders.h"
#include "mlir/IR/Module.h"
#include "mlir/IR/OpImplementation.h"
#include "mlir/IR/StandardTypes.h"
#include "mlir/Linalg/IR/LinalgTypes.h"
#include "mlir/Pass/Pass.h"
#include "mlir/StandardOps/Ops.h"
#include "mlir/Stencil/StencilOps.h"
#include "mlir/Support/Functional.h"
#include "mlir/Support/LLVM.h"
#include "mlir/Support/STLExtras.h"

using namespace mlir;

namespace {

class StencilVerticalRegionToLoopsPass
    : public ModulePass<StencilVerticalRegionToLoopsPass> {
public:
  void runOnLambdaOp(Stencil::LambdaOp op) {
    Type fieldType = op.getResult()->getType();
    OpBuilder rewriter(&op.getOperation()->getFunction().front().front());
    Stencil::TempOp tempOp =
        rewriter.create<Stencil::TempOp>(op.getLoc(), fieldType);
    rewriter.setInsertionPoint(op);

    Module m = op.getOperation()->getFunction().getModule();
    Function callee = m.getNamedFunction(op.callee());

    BlockAndValueMapping mapping;
    unsigned i = 0;
    for (Value *argument : callee.getArguments()) {
      mapping.map(argument, op.getOperand(i++));
    }

    Block &calleeBody = callee.getBody().front();
    for (Operation &calleeOp : calleeBody) {
      if (!isa<ReturnOp>(calleeOp)) {
        Operation *clonedOperation = rewriter.clone(calleeOp, mapping);
        if (isa<Stencil::TempOp>(*clonedOperation))
          clonedOperation->moveBefore(
              &op.getOperation()->getFunction().front().front());
      }
    }

    if (callee.getType().getNumResults() == 1) {
      ReturnOp returnOp = cast<ReturnOp>(calleeBody.getTerminator());
      Value *returnValue = returnOp.getOperand(0);
      rewriter.create<Stencil::WriteOp>(op.getLoc(), tempOp.getResult(),
                                        mapping.lookupOrDefault(returnValue));
    } else if (callee.getType().getNumResults() != 0)
      op.emitError("stencil functions cannot have more than one result");

    op.getResult()->replaceAllUsesWith(tempOp.getResult());
    op.erase();
  }

  void runOnModule() override {
    Module m = getModule();
    m.walk<Stencil::LambdaOp>([&](Stencil::LambdaOp op) { runOnLambdaOp(op); });

    ModuleManager moduleManager(m);
    llvm::SmallVector<Function, 4> stencilFunctions;
    for (Function f : m.getFunctions()) {
      if (f.getAttr("stencil.function"))
        stencilFunctions.push_back(f);
    }
    for (Function f : stencilFunctions)
      moduleManager.erase(f);

    for (Function f : m.getFunctions()) {
      if (f.getName() == "init_globals")
        continue;
      OpBuilder builder(&f.getBody().front().front());
      auto kstart = builder.create<Stencil::ContextOp>(
          builder.getUnknownLoc(), builder.getIndexType(),
          builder.getStringAttr("kstart"));
      auto kend = builder.create<Stencil::ContextOp>(
          builder.getUnknownLoc(), builder.getIndexType(),
          builder.getStringAttr("kend"));
      f.walk<Stencil::ContextOp>([&](Stencil::ContextOp op) {
        if (op.name() == "kstart" && op != kstart)
          op.getResult()->replaceAllUsesWith(kstart);
        if (op.name() == "kend" && op != kend)
          op.getResult()->replaceAllUsesWith(kend);
      });
    }
  }
};

static PassRegistration<StencilVerticalRegionToLoopsPass>
    pass("stencil-inline-lambdas",
         "Inline stencil lambda operations to only use reads and writes");

} // namespace

#include "mlir/IR/Builders.h"
#include "mlir/IR/Function.h"
#include "mlir/Pass/Pass.h"
#include "mlir/Stencil/StencilOps.h"
#include "mlir/Transforms/FoldUtils.h"

using namespace mlir;

namespace {

class StencilOffsetFoldPass : public FunctionPass<StencilOffsetFoldPass> {
public:
  void runOnFunction() override {
    // fold offset operations
    OperationFolder folder;
    Function f = getFunction();
    f.walk([&](Operation* op){
      if (isa<Stencil::OffsetAddOp>(op) || isa<Stencil::OffsetNegOp>(op))
        folder.tryToFold(op);
    });

    // eliminate FieldViewOp
    f.walk<Stencil::FieldViewOp>([&](Stencil::FieldViewOp op) {
      llvm::SmallVector<Operation *, 4> uses;
      for (auto &use : op.getResult()->getUses()) {
        uses.emplace_back(use.getOwner());
      }

      for (auto &use : uses) {
        if (auto readOp = dyn_cast<Stencil::ReadOp>(use)) {
          if (auto offOp = dyn_cast<Stencil::ConstantOffsetOp>(
                  op.offset()->getDefiningOp())) {
            if (auto offOp2 = dyn_cast<Stencil::ConstantOffsetOp>(
                    readOp.offset()->getDefiningOp())) {
              OpBuilder builder(readOp);
              auto newOff = builder.create<Stencil::ConstantOffsetOp>(
                  readOp.getLoc(),
                  offOp.i().getSExtValue() + offOp2.i().getSExtValue(),
                  offOp.j().getSExtValue() + offOp2.j().getSExtValue(),
                  offOp.k().getSExtValue() + offOp2.k().getSExtValue());
              readOp.getOperation()->setOperand(0, op.field());
              readOp.getOperation()->setOperand(1, newOff);
            } else {
              op.emitError("non-constant offset used in a read from a view");
            }
          } else {
            op.emitError("view can only be given a constant offset for now");
          }
        } else {
          op.emitError("view can only be used in read operations for now");
        }
      }
      op.getOperation()->erase();
    });
  }
};

static PassRegistration<StencilOffsetFoldPass>
    pass("stencil-offset-fold",
         "Fold offsets used in stencil.read operations");

} // namespace
